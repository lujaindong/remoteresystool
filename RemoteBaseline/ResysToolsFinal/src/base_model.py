"""define base class model"""
import abc
import math
import tensorflow as tf
import utils.util as util
from IO.iterator import BaseIterator

__all__ = ["BaseModel"]


class BaseModel(object):
    def __init__(self, hparams, mode, iterator, scope=None):
        assert isinstance(iterator, BaseIterator)
        self.iterator = iterator
        self.mode = mode
        self.layer_params = []
        self.feature_cnt = self._load_feature_cnt(hparams)
        with tf.variable_scope("embedding"):
            self.embedding = tf.get_variable(name='embedding_layer', \
                                             shape=[hparams.FEATURE_COUNT, hparams.dim], \
                                             dtype=tf.float32, \
                                             initializer=tf.truncated_normal_initializer( \
                                                 mean=0, \
                                                 stddev=hparams.init_value \
                                                        / math.sqrt(float(hparams.dim))))
        self.initializer = self._get_initializer(hparams)
        self.logit = self._build_graph(hparams)
        self.pred = tf.sigmoid(self.logit)
        self.pred_loss = self._compute_pred_loss(hparams)
        self.regular_loss = self._compute_regular_loss(hparams)
        self.loss = tf.add(self.pred_loss, self.regular_loss)
        self.saver = tf.train.Saver(max_to_keep=hparams.epochs)
        if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
            self.update = self._build_train_opt(hparams)
            self.init_op = tf.global_variables_initializer()

        #self.merged = self._add_summaries()

    def _add_summaries(self):
        tf.summary.scalar("predict_loss", self.pred_loss)
        tf.summary.scalar("regular_loss", self.regular_loss)
        tf.summary.scalar("loss", self.loss)
        tf.summary.histogram("predict prob", self.pred)
        tf.summary.histogram("embedding", self.embedding)
        merged = tf.summary.merge_all()
        return merged

    @abc.abstractmethod
    def _build_graph(self, hparams):
        """Subclass must implement this."""
        pass

    def _load_feature_cnt(self, hparams):
        if hparams.regular_type == 'common_regular':
            return
        indices = []
        vals = []
        with open(util.FEAT_COUNT_FILE, 'r') as f_in:
            for line in f_in:
                tmp = line.strip().split(',')
                if len(tmp) < 2:
                    continue
                indices.append([0, int(tmp[0])])
                if hparams.regular_type == 'limu_regular':
                    vals.append(float(tmp[1]))
                elif hparams.regular_type == 'din_regular':
                    vals.append(1.0 / float(tmp[1]))
                elif hparams.regular_type == 'sparse_regular':
                    vals.append(1.0)

        tmp = tf.SparseTensor(indices, vals, dense_shape=[1, hparams.FEATURE_COUNT])
        # 使用0.1来填充,避免计算正则，出现除0
        tmp = tf.sparse_tensor_to_dense(sp_input=tmp, default_value=0.1)
        tmp = tf.transpose(tmp)
        return tmp

    # 实现稀疏的正则化方式
    def _weighted_l2_regular(self, hparams):
        print('using sparse weight {0} l2 regularization!'.format(hparams.regular_type))
        if hparams.regular_type == 'sparse_regular':
            batch_weight = tf.gather(tf.pow(self.embedding, 2), self.iterator.batch_feat_index)
        else:
            batch_weight = tf.gather(tf.multiply(self.feature_cnt, tf.pow(self.embedding, 2)),
                                     self.iterator.batch_feat_index)
        sparse_l2_loss = tf.multiply(hparams.embed_l2, tf.norm(batch_weight, ord=2))
        params = self.layer_params
        for param in params:
            sparse_l2_loss = tf.add(sparse_l2_loss, \
                                    tf.multiply(hparams.layer_l2, tf.norm(param, ord=2)))
        return sparse_l2_loss

    def _weighted_l1_regular(self, hparams):
        print('using sparse weight {0} l1 regularization!'.format(hparams.regular_type))
        if hparams.regular_type == 'sparse_regular':
            batch_weight = tf.gather(self.embedding, self.iterator.batch_feat_index)
        else:
            batch_weight = tf.gather(tf.multiply(self.feature_cnt, self.embedding), \
                                     self.iterator.batch_feat_index)
        sparse_l1_loss = tf.multiply(hparams.embed_l1, tf.norm(batch_weight, ord=1))
        params = self.layer_params
        for param in params:
            sparse_l1_loss = tf.add(sparse_l1_loss, tf.multiply(hparams.layer_l1, \
                                                                tf.norm(param, ord=1)))
        return sparse_l1_loss

    def _l2_loss(self, hparams):
        l2_loss = tf.zeros([1], dtype=tf.float32)
        # embedding_layer l2 loss
        l2_loss = tf.add(l2_loss, tf.multiply(hparams.embed_l2, tf.nn.l2_loss(self.embedding)))
        params = self.layer_params
        for param in params:
            l2_loss = tf.add(l2_loss, tf.multiply(hparams.layer_l2, tf.nn.l2_loss(param)))
        return l2_loss

    def _l1_loss(self, hparams):
        l1_loss = tf.zeros([1], dtype=tf.float32)
        # embedding_layer l2 loss
        l1_loss = tf.add(l1_loss, tf.multiply(hparams.embed_l1, tf.norm(self.embedding, ord=1)))
        params = self.layer_params
        for param in params:
            l1_loss = tf.add(l1_loss, tf.multiply(hparams.layer_l1, tf.norm(param, ord=1)))
        return l1_loss

    def _get_initializer(self, hparams):
        if hparams.init_method == 'tnormal':
            return tf.truncated_normal_initializer(stddev=hparams.init_value)
        elif hparams.init_method == 'uniform':
            return tf.random_uniform_initializer(-hparams.init_value, hparams.init_value)
        elif hparams.init_method == 'normal':
            return tf.random_normal_initializer(stddev=hparams.init_value)
        elif hparams.init_method == 'xavier_normal':
            return tf.contrib.layers.xavier_initializer(uniform=False)
        elif hparams.init_method == 'xavier_uniform':
            return tf.contrib.layers.xavier_initializer(uniform=True)
        elif hparams.init_method == 'he_normal':
            return tf.contrib.layers.variance_scaling_initializer( \
                factor=2.0, mode='FAN_IN', uniform=False)
        elif hparams.init_method == 'he_uniform':
            return tf.contrib.layers.variance_scaling_initializer( \
                factor=2.0, mode='FAN_IN', uniform=True)
        else:
            return tf.truncated_normal_initializer(stddev=hparams.init_value)

    def _compute_pred_loss(self, hparams):
        if hparams.loss == 'cross_entropy_loss':
            pred_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits( \
                logits=tf.reshape(self.logit, [-1]), \
                labels=tf.reshape(self.iterator.labels, [-1])))
        elif hparams.loss == 'square_loss':
            pred_loss = tf.reduce_mean(tf.squared_difference( \
                tf.sigmoid(self.logit), self.iterator.labels))
        elif hparams.loss == 'log_loss':
            pred_loss = tf.reduce_mean(
                tf.losses.log_loss( \
                    predictions=tf.sigmoid(self.logit), labels=self.iterator.labels))
        else:
            raise ValueError("this loss not defined {0}".format(hparams.loss))
        return pred_loss

    def _compute_regular_loss(self, hparams):
        print('embedding param:', self.embedding)
        print('layer params:', self.layer_params)
        if hparams.regular_type == 'common_regular':
            regular_loss = self._l2_loss(hparams) + self._l1_loss(hparams)
        else:
            regular_loss = self._weighted_l2_regular(hparams) + self._weighted_l1_regular(hparams)
        regular_loss = tf.reduce_sum(regular_loss)
        return regular_loss

    def _build_train_opt(self, hparams):
        def train_opt(hparams):
            if hparams.optimizer == 'adadelta':
                train_step = tf.train.AdadeltaOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'adagrad':
                train_step = tf.train.AdagradOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'sgd':
                train_step = tf.train.GradientDescentOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'adam':
                train_step = tf.train.AdamOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'ftrl':
                train_step = tf.train.FtrlOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'gd':
                train_step = tf.train.GradientDescentOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'padagrad':
                train_step = tf.train.ProximalAdagradOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'pgd':
                train_step = tf.train.ProximalGradientDescentOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            elif hparams.optimizer == 'rmsprop':
                train_step = tf.train.RMSPropOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            else:
                train_step = tf.train.GradientDescentOptimizer( \
                    hparams.learning_rate).minimize(self.loss)
            return train_step

        #use batch normalization!
        if True in hparams.batch_norm:
            print("using batch normalization!")
            update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
            with tf.control_dependencies(update_ops):
                train_step = train_opt(hparams)
        else:
            train_step = train_opt(hparams)
        return train_step

    # 将BN层和dropout层封装在一起
    def _active_layer(self, logit, scope, activation, dropout, is_bn):
        logit = self._batch_norm(logit, scope, is_bn)
        logit = self._dropout(logit, dropout)
        logit = self._activate(logit, activation)
        return logit

    # 激活函数
    def _activate(self, logit, activation):
        if activation == 'sigmoid':
            return tf.nn.sigmoid(logit)
        elif activation == 'softmax':
            return tf.nn.softmax(logit)
        elif activation == 'relu':
            return tf.nn.relu(logit)
        elif activation == 'tanh':
            return tf.nn.tanh(logit)
        elif activation == 'elu':
            return tf.nn.elu(logit)
        else:
            raise ValueError("this activations not defined {0}".format(activation))

    # dropout layer
    def _dropout(self, logit, dropout):
        if self.mode == tf.contrib.learn.ModeKeys.TRAIN and dropout > 0.0:
            print('user dropout!!!!!!')
            keep_prob = 1.0 - dropout
            logit = tf.nn.dropout(x=logit, keep_prob=keep_prob)
            return logit
        else:
            return logit

    # batch norm layer
    def _batch_norm(self, logit, scope, is_bn):
        if is_bn:
            if self.mode == tf.contrib.learn.ModeKeys.TRAIN:
                return tf.contrib.layers.batch_norm( \
                    logit, is_training=True, reuse=None, scope=scope)
            else:
                return tf.contrib.layers.batch_norm( \
                    logit, is_training=False, reuse=None, scope=scope)
        else:
            return logit

    def train(self, sess):
        assert self.mode == tf.contrib.learn.ModeKeys.TRAIN
        return sess.run([self.update, self.loss, \
                         self.pred_loss])

    def eval(self, sess):
        assert self.mode == tf.contrib.learn.ModeKeys.EVAL
        return sess.run([self.loss, self.pred_loss, \
                         self.pred, self.iterator.labels])

    def infer(self, sess):
        assert self.mode == tf.contrib.learn.ModeKeys.INFER
        return sess.run([self.pred])
