"""define train, infer, eval process"""

import collections
import numpy as np
import os
import time
import tensorflow as tf
from IO.iterator import FfmIterator, DinIterator
from IO.din_cache import DinCache
from IO.ffm_cache import FfmCache
from src.deep_fm import DeepfmModel
from src.deep_wide import DeepWideModel
from src.dnn import DnnModel
from src.pnn import PnnModel
from src.din import DinModel
from src.fm import FmModel
import utils.util as util
import utils.metric as metric
from utils.log import logger


# from hparams import *
class TrainModel(collections.namedtuple("TrainModel", ("graph", "model", "iterator"))):
    """define train class, include graph, model, iterator"""
    pass


def create_train_model(model_creator, hparams, train_file, scope=None):
    graph = tf.Graph()
    with graph.as_default():
        filenames = [train_file]
        src_dataset = tf.contrib.data.TFRecordDataset(filenames)
        # 构建训练数据的迭代器
        if hparams.data_format == 'din':
            batch_input = DinIterator(src_dataset)
        elif hparams.data_format == 'ffm':
            batch_input = FfmIterator(src_dataset)
        else:
            raise ValueError("not support {0} format data".format(hparams.data_format))
        # 创建model
        model = model_creator(
            hparams,
            mode=tf.contrib.learn.ModeKeys.TRAIN,
            iterator=batch_input,
            scope=scope)
    # 返回train graph, 含有graph, model, iterator
    return TrainModel(
        graph=graph,
        model=model,
        iterator=batch_input)


class EvalModel(collections.namedtuple("EvalModel", ("graph", "model", "iterator"))):
    """define eval class, include graph, model, iterator"""
    pass


def create_eval_model(model_creator, hparams, eval_file, scope=None):
    graph = tf.Graph()
    with graph.as_default():
        filenames = [eval_file]
        src_dataset = tf.contrib.data.TFRecordDataset(filenames)
        # 构建训练数据的迭代器
        if hparams.data_format == 'din':
            batch_input = DinIterator(src_dataset)
        elif hparams.data_format == 'ffm':
            batch_input = FfmIterator(src_dataset)
        else:
            raise ValueError("not support {0} format data".format(hparams.data_format))
        # 创建model
        model = model_creator(
            hparams,
            mode=tf.contrib.learn.ModeKeys.EVAL,
            iterator=batch_input,
            scope=scope)
    # 返回train graph, 含有graph, model, iterator
    return EvalModel(
        graph=graph,
        model=model,
        iterator=batch_input)


class InferModel(collections.namedtuple("InferModel", ("graph", "model", "iterator"))):
    """define infer class, include graph, model, iterator"""
    pass


def create_infer_model(model_creator, hparams, infer_file, scope=None):
    graph = tf.Graph()
    with graph.as_default():
        filenames = [infer_file]
        src_dataset = tf.contrib.data.TFRecordDataset(filenames)
        # 构建训练数据的迭代器
        if hparams.data_format == 'din':
            batch_input = DinIterator(src_dataset)
        elif hparams.data_format == 'ffm':
            batch_input = FfmIterator(src_dataset)
        else:
            raise ValueError("not support {0} format data".format(hparams.data_format))
        # 创建model
        model = model_creator(
            hparams,
            mode=tf.contrib.learn.ModeKeys.INFER,
            iterator=batch_input,
            scope=scope)
    # 返回train graph, 含有graph, model, iterator
    return InferModel(
        graph=graph,
        model=model,
        iterator=batch_input)


# run evaluation and get evaluted loss
def run_eval(load_model, load_sess, model_dir, sample_num_file, hparams, flag):
    # load sample num
    with open(sample_num_file, 'r') as f:
        sample_num = int(f.readlines()[0].strip())
    load_model.model.saver.restore(load_sess, model_dir)
    load_sess.run(load_model.iterator.initializer)
    preds = []
    labels = []
    while True:
        try:
            _, _, step_pred, step_labels = load_model.model.eval(load_sess)
            preds.extend(np.reshape(step_pred, -1))
            labels.extend(np.reshape(step_labels, -1))
        except tf.errors.OutOfRangeError:
            break
    preds = preds[:sample_num]
    labels = labels[:sample_num]
    logger.info("data num:{0:d}".format(len(labels)))
    res = metric.cal_metric(labels, preds, hparams, flag)
    return res


# run infer
def run_infer(load_model, load_sess, model_dir, hparams, sample_num_file):
    # load sample num
    with open(sample_num_file, 'r') as f:
        sample_num = int(f.readlines()[0].strip())
    if not os.path.exists(util.RES_DIR):
        os.mkdir(util.RES_DIR)
    load_model.model.saver.restore(load_sess, model_dir)
    load_sess.run(load_model.iterator.initializer)
    preds = []
    while True:
        try:
            step_pred = load_model.model.infer(load_sess)
            preds.extend(np.reshape(step_pred, -1))
        except tf.errors.OutOfRangeError:
            break
    preds = preds[:sample_num]
    hparams.res_name = util.convert_res_name(hparams.infer_file)
    print('result name:', hparams.res_name)
    with open(hparams.res_name, 'w') as out:
        out.write('\n'.join(map(str, preds)))


def cache_train_data(hparams):
    if hparams.data_format == 'din':
        cache_obj = DinCache()
    elif hparams.data_format == 'ffm':
        cache_obj = FfmCache()
    else:
        raise ValueError("this format not defined {0}".format(hparams.data_format))
    if not os.path.exists(util.CACHE_DIR):
        os.mkdir(util.CACHE_DIR)
    if not hparams.train_file is None:
        hparams.train_file_cache = util.convert_cached_name(hparams.train_file, hparams.batch_size)
        print('train_cache:', hparams.train_file_cache)
        if not os.path.isfile(hparams.train_file_cache):
            print('has not cached train file, begin cached...')
            start_time = time.time()
            sample_num, impression_id_list = cache_obj.write_tfrecord(hparams.train_file,
                                                                      hparams.train_file_cache,
                                                                      hparams)
            util.print_time("caced train file used time", start_time)
            print("train data sample num:{0}".format(sample_num))
            with open(util.TRAIN_NUM, 'w') as f:
                f.write(str(sample_num) + '\n')
            with open(util.TRAIN_IMPRESSION_ID, 'w') as f:
                for impression_id in impression_id_list:
                    f.write(str(impression_id) + '\n')


def cache_eval_data(hparams):
    if hparams.data_format == 'din':
        cache_obj = DinCache()
    elif hparams.data_format == 'ffm':
        cache_obj = FfmCache()
    else:
        raise ValueError("this format not defined {0}".format(hparams.data_format))
    if not hparams.eval_file is None:
        hparams.eval_file_cache = util.convert_cached_name(hparams.eval_file, hparams.batch_size)
        print('eval_cache:', hparams.eval_file_cache)
        if not os.path.isfile(hparams.eval_file_cache):
            print('has not cached eval file, begin cached...')
            start_time = time.time()
            sample_num, impression_id_list = cache_obj.write_tfrecord(hparams.eval_file,
                                                                      hparams.eval_file_cache,
                                                                      hparams)
            util.print_time("caced eval file used time", start_time)
            print("eval data sample num:{0}".format(sample_num))
            with open(util.EVAL_NUM, 'w') as f:
                f.write(str(sample_num) + '\n')
            with open(util.EVAL_IMPRESSION_ID, 'w') as f:
                for impression_id in impression_id_list:
                    f.write(str(impression_id) + '\n')


def cache_infer_data(hparams):
    if hparams.data_format == 'din':
        cache_obj = DinCache()
    elif hparams.data_format == 'ffm':
        cache_obj = FfmCache()
    else:
        raise ValueError("this format not defined {0}".format(hparams.data_format))
    if not hparams.infer_file is None:
        hparams.infer_file_cache = util.convert_cached_name(hparams.infer_file,
                                                            hparams.batch_size)
        print('infer_cache:', hparams.infer_file_cache)
        if not os.path.isfile(hparams.infer_file_cache):
            print('has not cached infer file, begin cached...')
            start_time = time.time()
            sample_num, impression_id_list = cache_obj.write_tfrecord(hparams.infer_file,
                                                                      hparams.infer_file_cache,
                                                                      hparams)
            util.print_time("caced infer file used time", start_time)
            print("infer data sample num:{0:d}".format(sample_num))
            with open(util.INFER_NUM, 'w') as f:
                f.write(str(sample_num) + '\n')
            with open(util.INFER_IMPRESSION_ID, 'w') as f:
                for impression_id in impression_id_list:
                    f.write(str(impression_id) + '\n')


def train(hparams, scope=None, target_session=""):
    params = hparams.values()
    for key, val in params.items():
        logger.info(str(key) + ':' + str(val))
    print('load and cache data...')
    cache_train_data(hparams)
    cache_eval_data(hparams)
    if hparams.infer_file is not None:
        cache_infer_data(hparams)

    if hparams.model_type == 'deepFM':
        model_creator = DeepfmModel
        print("use deepfm model!")
    elif hparams.model_type == 'deepWide':
        model_creator = DeepWideModel
        print("use deepWide model!")
    elif hparams.model_type == 'dnn':
        print("use dnn model!")
        model_creator = DnnModel
    elif hparams.model_type == 'pnn':
        print("use pnn model!")
        model_creator = PnnModel
    elif hparams.model_type == 'din':
        print("use din model!")
        model_creator = DinModel
    elif hparams.model_type == 'fm':
        print("use fm model!")
        model_creator = FmModel
    else:
        raise ValueError("model type should be deepFM, deepWide, dnn")

    # define train,eval,infer graph
    # define train session, eval session, infer session
    train_model = create_train_model(model_creator, hparams, hparams.train_file_cache, scope)
    train_sess = tf.Session(target=target_session, graph=train_model.graph)

    eval_model_eval = create_eval_model(model_creator, hparams, hparams.eval_file_cache, scope)
    eval_eval_sess = tf.Session(target=target_session, graph=eval_model_eval.graph)
    eval_model_train = create_eval_model(model_creator, hparams, hparams.train_file_cache, scope)
    eval_train_sess = tf.Session(target=target_session, graph=eval_model_train.graph)

    if hparams.infer_file is not None:
        infer_model = create_infer_model(model_creator, hparams, hparams.infer_file_cache, scope)
        infer_sess = tf.Session(target=target_session, graph=infer_model.graph)

    train_sess.run(train_model.model.init_op)
    # load model from checkpoint
    if not hparams.load_model_name is None:
        checkpoint_path = hparams.load_model_name
        try:
            train_model.model.saver.restore(train_sess, checkpoint_path)
        except:
            raise IOError("Failed to find any matching files for {0}".format(checkpoint_path))
        else:
            print('load model', checkpoint_path)
    step = 0
    #writer = tf.summary.FileWriter(util.SUMMARIES_DIR, train_sess.graph)
    for epoch in range(hparams.epochs):
        train_sess.run(train_model.iterator.initializer)
        epoch_loss = 0
        train_start = time.time()
        load_time = 0
        train_load_time = 0
        while True:
            try:
                t1 = time.time()
                t2 = time.time()
                step_result = train_model.model.train(train_sess)
                t3 = time.time()
                load_time += t2 - t1
                train_load_time += t3 - t2
                (_, step_loss, step_pred_loss) = step_result
                #writer.add_summary(summary, step)
                epoch_loss += step_loss
                step += 1
                if step % hparams.show_step == 0:
                    print('at step {0:d} , total loss: {1:.4f}, logloss: {2:.4f}' \
                          .format(step, step_loss, step_pred_loss))
            except tf.errors.OutOfRangeError:
                print('finish one epoch!')
                break
        train_end = time.time()
        train_time = train_end - train_start
        checkpoint_path = train_model.model.saver.save(
            sess=train_sess,
            save_path=util.MODEL_DIR + 'epoch_' + str(epoch))
        print(checkpoint_path)
        eval_start = time.time()
        train_res = run_eval(eval_model_train, eval_train_sess,
                             checkpoint_path, util.TRAIN_NUM,
                             hparams, flag='train')
        eval_res = run_eval(eval_model_eval, eval_eval_sess,
                            checkpoint_path, util.EVAL_NUM,
                            hparams, flag='eval')
        eval_end = time.time()
        eval_time = eval_end - eval_start
        train_info = ', '.join(
            [str(item[0]) + ':' + str(item[1])
             for item in sorted(train_res.items(), key=lambda x: x[0])])
        eval_info = ', '.join(
            [str(item[0]) + ':' + str(item[1])
             for item in sorted(eval_res.items(), key=lambda x: x[0])])
        # print('at epoch {0:d} , train loss: {1:.4f}, train auc: {2:.4f}, val loss: {3:.4f}, val auc: {4:.4f}'.format(epoch, train_logloss, train_auc, eval_logloss, eval_auc))
        # print('at epoch {0:d} , train time: {1:.1f} eval time: {2:.1f}'.format(epoch, train_time, eval_time))
        # print('at epoch {0:d} , load data time: {1:.1f} train and load time: {2:.1f}'.format(epoch, load_time, train_load_time))
        logger.info('at epoch {0:d}'.format(epoch) + ' train info: ' \
                    + train_info + ' eval info: ' + eval_info)
        logger.info('at epoch {0:d} , train time: {1:.1f} eval time: {2:.1f}' \
                    .format(epoch, train_time, eval_time))
        logger.info('at epoch {0:d} , load data time: {1:.1f} train and load time: {2:.1f}' \
                    .format(epoch, load_time, train_load_time))
        logger.info('\n')
    #writer.close()
    # after train,run infer
    if hparams.infer_file is not None:
        run_infer(infer_model, infer_sess, checkpoint_path, hparams, util.INFER_NUM)


def test_train():
    # 训练的过程中创建了参数
    check_tensorflow_version()
    check_and_mkdir()
    hparams = create_hparams()
    if hparams.method == 'Train':
        # 将参数保存到model文件夹当中, predict需要将该参数继续载入,来build graph
        save_hparams(util.MODEL_DIR, hparams)
        train(hparams)
    else:
        # 加载train保存的hparams
        hparams_prev = load_hparams(util.MODEL_DIR)
        hparams = extend_hparams(hparams, hparams_prev)
        infer(hparams)

        # test_train()
