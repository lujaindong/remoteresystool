"""define iterator"""
import collections
import tensorflow as tf
import abc

BUFFER_SIZE = 256
__all__ = ["BaseIterator", "FfmIterator", "DinIterator"]


class BaseIterator(object):
    @abc.abstractmethod
    def get_iterator(self, src_dataset):
        """Subclass must implement this."""
        pass

    @abc.abstractmethod
    def parser(self, record):
        pass


class DinIterator(BaseIterator):
    def __init__(self, src_dataset):
        self.get_iterator(src_dataset)

    def get_iterator(self, src_dataset):
        src_dataset = src_dataset.map(self.parser)
        src_dataset = src_dataset.shuffle(buffer_size=BUFFER_SIZE)
        iterator = src_dataset.make_initializable_iterator()
        output = iterator.get_next()
        (_attention_news_indices, _attention_news_values, _attention_news_shape, \
         _attention_user_indices, _attention_user_values, _attention_user_weights, \
         _attention_user_shape, _fm_feat_indices, _fm_feat_val, _batch_feat_index, \
         _fm_feat_shape, _labels, _dnn_feat_indices, _dnn_feat_values, \
         _dnn_feat_weight, _dnn_feat_shape) = output
        self.initializer = iterator.initializer
        self.attention_news_indices = _attention_news_indices
        self.attention_news_values = _attention_news_values
        self.attention_news_shape = _attention_news_shape
        self.attention_user_indices = _attention_user_indices
        self.attention_user_values = _attention_user_values
        self.attention_user_weights = _attention_user_weights
        self.attention_user_shape = _attention_user_shape
        self.fm_feat_indices = _fm_feat_indices
        self.fm_feat_val = _fm_feat_val
        self.batch_feat_index = _batch_feat_index
        self.fm_feat_shape = _fm_feat_shape
        self.labels = _labels
        self.dnn_feat_indices = _dnn_feat_indices
        self.dnn_feat_values = _dnn_feat_values
        self.dnn_feat_weight = _dnn_feat_weight
        self.dnn_feat_shape = _dnn_feat_shape

    def parser(self, record):
        keys_to_features = {
            'attention_news_indices': tf.FixedLenFeature([], tf.string),
            'attention_news_values': tf.VarLenFeature(tf.float32),
            'attention_news_shape': tf.FixedLenFeature([2], tf.int64),

            'attention_user_indices': tf.FixedLenFeature([], tf.string),
            'attention_user_values': tf.VarLenFeature(tf.int64),
            'attention_user_weights': tf.VarLenFeature(tf.float32),
            'attention_user_shape': tf.FixedLenFeature([2], tf.int64),

            'fm_feat_indices': tf.FixedLenFeature([], tf.string),
            'fm_feat_val': tf.VarLenFeature(tf.float32),
            'batch_feat_index': tf.VarLenFeature(tf.int64),
            'fm_feat_shape': tf.FixedLenFeature([2], tf.int64),

            'labels': tf.FixedLenFeature([], tf.string),

            'dnn_feat_indices': tf.FixedLenFeature([], tf.string),
            'dnn_feat_values': tf.VarLenFeature(tf.int64),
            'dnn_feat_weight': tf.VarLenFeature(tf.float32),
            'dnn_feat_shape': tf.FixedLenFeature([2], tf.int64),
        }
        parsed = tf.parse_single_example(record, keys_to_features)

        attention_news_indices = tf.reshape(tf.decode_raw(parsed['attention_news_indices'], \
                                                          tf.int64), [-1, 2])
        attention_news_values = tf.sparse_tensor_to_dense(parsed['attention_news_values'])
        attention_news_shape = parsed['attention_news_shape']

        attention_user_indices = tf.reshape(tf.decode_raw(parsed['attention_user_indices'], \
                                                          tf.int64), [-1, 2])
        attention_user_values = tf.sparse_tensor_to_dense(parsed['attention_user_values'])
        attention_user_weights = tf.sparse_tensor_to_dense(parsed['attention_user_weights'])
        attention_user_shape = parsed['attention_user_shape']

        fm_feat_indices = tf.reshape(tf.decode_raw(parsed['fm_feat_indices'], \
                                                   tf.int64), [-1, 2])
        fm_feat_val = tf.sparse_tensor_to_dense(parsed['fm_feat_val'])
        batch_feat_index = tf.sparse_tensor_to_dense(parsed['batch_feat_index'])
        fm_feat_shape = parsed['fm_feat_shape']

        labels = tf.reshape(tf.decode_raw(parsed['labels'], tf.float32), [-1, 1])

        dnn_feat_indices = tf.reshape(tf.decode_raw(parsed['dnn_feat_indices'], \
                                                    tf.int64), [-1, 2])
        dnn_feat_values = tf.sparse_tensor_to_dense(parsed['dnn_feat_values'])
        dnn_feat_weight = tf.sparse_tensor_to_dense(parsed['dnn_feat_weight'])
        dnn_feat_shape = parsed['dnn_feat_shape']
        return (attention_news_indices, attention_news_values, attention_news_shape, \
                attention_user_indices, attention_user_values, attention_user_weights, \
                attention_user_shape, fm_feat_indices, fm_feat_val, batch_feat_index, \
                fm_feat_shape, labels, dnn_feat_indices, dnn_feat_values, \
                dnn_feat_weight, dnn_feat_shape)


class FfmIterator(BaseIterator):
    def __init__(self, src_dataset):
        self.get_iterator(src_dataset)

    def get_iterator(self, src_dataset):
        src_dataset = src_dataset.map(self.parser)
        src_dataset = src_dataset.shuffle(buffer_size=BUFFER_SIZE)
        iterator = src_dataset.make_initializable_iterator()
        _fm_feat_indices, _fm_feat_values, _batch_feat_index, \
        _fm_feat_shape, _labels, _dnn_feat_indices, \
        _dnn_feat_values, _dnn_feat_weights, _dnn_feat_shape = iterator.get_next()
        self.initializer = iterator.initializer
        self.fm_feat_indices = _fm_feat_indices
        self.fm_feat_values = _fm_feat_values
        self.batch_feat_index = _batch_feat_index
        self.fm_feat_shape = _fm_feat_shape
        self.labels = _labels
        self.dnn_feat_indices = _dnn_feat_indices
        self.dnn_feat_values = _dnn_feat_values
        self.dnn_feat_weights = _dnn_feat_weights
        self.dnn_feat_shape = _dnn_feat_shape

    def parser(self, record):
        keys_to_features = {
            'fm_feat_indices': tf.FixedLenFeature([], tf.string),
            'fm_feat_values': tf.VarLenFeature(tf.float32),
            'batch_feat_index': tf.VarLenFeature(tf.int64),
            'fm_feat_shape': tf.FixedLenFeature([2], tf.int64),
            'labels': tf.FixedLenFeature([], tf.string),
            'dnn_feat_indices': tf.FixedLenFeature([], tf.string),
            'dnn_feat_values': tf.VarLenFeature(tf.int64),
            'dnn_feat_weights': tf.VarLenFeature(tf.float32),
            'dnn_feat_shape': tf.FixedLenFeature([2], tf.int64),
        }
        parsed = tf.parse_single_example(record, keys_to_features)
        fm_feat_indices = tf.reshape(tf.decode_raw(parsed['fm_feat_indices'], tf.int64), [-1, 2])
        fm_feat_values = tf.sparse_tensor_to_dense(parsed['fm_feat_values'])
        batch_feat_index = tf.sparse_tensor_to_dense(parsed['batch_feat_index'])
        fm_feat_shape = parsed['fm_feat_shape']
        labels = tf.reshape(tf.decode_raw(parsed['labels'], tf.float32), [-1, 1])
        dnn_feat_indices = tf.reshape(tf.decode_raw(parsed['dnn_feat_indices'], tf.int64), [-1, 2])
        dnn_feat_values = tf.sparse_tensor_to_dense(parsed['dnn_feat_values'])
        dnn_feat_weights = tf.sparse_tensor_to_dense(parsed['dnn_feat_weights'])
        dnn_feat_shape = parsed['dnn_feat_shape']
        return fm_feat_indices, fm_feat_values, batch_feat_index, \
               fm_feat_shape, labels, dnn_feat_indices, \
               dnn_feat_values, dnn_feat_weights, dnn_feat_shape


# 测试代码
def test_DinIterator():
    outfile = '../cache/batch_size_15_train.entertainment.no_inter.norm.fieldwise.attention.toy.tfrecord'
    filenames = [outfile]
    src_dataset = tf.contrib.data.TFRecordDataset(filenames)
    batch_iter = DinIterator(src_dataset)
    print(batch_iter.attention_news_shape)
    # sess = tf.Session()
    # for _ in range(2):
    #     sess.run(batch_iter.initializer)
    #     while True:
    #         try:
    #             print(sess.run([batch_iter.fm_feat_indices, batch_iter.fm_feat_val, batch_iter.fm_feat_val_square]))
    #         except tf.errors.OutOfRangeError:
    #             print("end")
    #             break


# test_DinIterator()


def test_FfmIterator():
    outfile = '../cache/train.all.norm.fieldwise.toy.tfrecord'
    filenames = [outfile]
    src_dataset = tf.contrib.data.TFRecordDataset(filenames)
    batch_iter = FfmIterator(src_dataset)
    print(batch_iter.dnn_feat_indices)
    # sess = tf.Session()
    # for _ in range(2):
    #     sess.run(batch_iter.initializer)
    #     while True:
    #         try:
    #             print(sess.run([batch_iter.labels, batch_iter.dnn_feat_indices, batch_iter.dnn_feat_values, batch_iter.dnn_feat_weights, batch_iter.dnn_feat_shape]))
    #         except tf.errors.OutOfRangeError:
    #             print("end")
    #             break

# test_FfmIterator()
