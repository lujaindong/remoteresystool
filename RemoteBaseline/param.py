PARAMS = {'model_param':
              {'--model_type': ['deepFM', 'fm', 'deepWide'],  # 'deepFM','deepWide','pnn','fm'
               '--dim': [10, 16, 32, 50, 100, 150, 200],
               '--learning_rate': [0.0001],
               '--train_file': ['data/train_final_ffm_0.1_norm.csv'],
               '--eval_file': ['data/test_final_ffm_0.1_norm.csv']}
          }
